import os, sys, glob

vcfFilePath = os.path.expanduser(sys.argv[1])
rnaSeqSamplesFilePath = os.path.expanduser(sys.argv[2])
taskSeriesFilePath = os.path.expanduser(sys.argv[3])
variantsPerTaskSeries = int(sys.argv[4])
localParamFilePrefix = sys.argv[5]
outParamFilePrefix = sys.argv[6]
predictCommand = sys.argv[7]
resultFilePrefix = sys.argv[8]

#print "Counting lines in %s" % vcfFilePath
#numVcfLines = 0
#for line in file(vcfFilePath):
#    numVcfLines += 1
#    if numVcfLines % 100 == 0:
#        print numVcfLines

rnaSeqSamples = [line.rstrip() for line in file(rnaSeqSamplesFilePath)]

rawGenotypeMasterDict = {"0|0": "HC", "0|1": "HT", "1|0": "HT", "1|1": "HR"}

inLineCount = 0
outLineCount = 0
paramLines = []
paramFileCount = 0

def saveGroup(paramLines, paramFileCount):
    print "Saving group file %i" % paramFileCount
    localParamFilePath = "%sgroup%s" % (localParamFilePrefix, paramFileCount)
    outParamFilePath = "%sgroup%s" % (outParamFilePrefix, paramFileCount)

    paramFile = open(os.path.expanduser(localParamFilePath), 'w')
    paramFile.write("\n".join(paramLines) + "\n")
    paramFile.close()

    resultFilePath = "%sgroup%s" % (resultFilePrefix, paramFileCount)
    taskSeriesFile.write("%s %s %s\n" % (predictCommand, outParamFilePath, resultFilePath))

taskSeriesFile = open(taskSeriesFilePath, 'a')

for line in file(vcfFilePath):
    inLineCount += 1
    if inLineCount % variantsPerTaskSeries == 0:
        print "Processed line %i in %s" % (inLineCount, vcfFilePath)

    if line.startswith("#"):
        if line.startswith("#CHROM"):
            headerItems = line.rstrip().split("\t")
            sampleIDs = headerItems[9:]
        continue

    lineItems = line.rstrip().split("\t")

    if lineItems[6] != "PASS":
        continue

    #metaDict = {}
    #for x in lineItems[7].split(";"):
    #    y = x.split("=")
    #    metaDict[y[0]] = y[1]
    #af = metaDict["AF"]

    positionID = "chr%s__%s" % (lineItems[0], lineItems[1])

    sampleGenotypeDict = {}
    genotypeSampleDict = {"HC": [], "HT": [], "HR": []}
    for i in range(len(sampleIDs)):
        sampleID = sampleIDs[i]

        if not sampleID in rnaSeqSamples:
            continue

        rawGenotype = lineItems[9 + i].split(":")[0]

        if rawGenotype == ".":
            continue

        genotype = rawGenotypeMasterDict[rawGenotype]
        sampleGenotypeDict[sampleID] = genotype
        genotypeSampleDict[genotype].append(sampleID)

    numHC = len(genotypeSampleDict["HC"])
    numHT = len(genotypeSampleDict["HT"])
    numHR = len(genotypeSampleDict["HR"])

    def addParamLine(paramLines, positionID, description, class1Samples, class2Samples):
        numClass1 = float(len(class1Samples))
        numClass2 = float(len(class2Samples))
        total = numClass1 + numClass2
        minorProportionClass1 = numClass1 / total
        minorProportionClass1 = min([minorProportionClass1, 1 - minorProportionClass1])

        #if minorProportionClass1 > 0.01 and minorProportionClass1 <= 0.05:
        if numClass1 >=3 and numClass2 >= 3 and minorProportionClass1 < 0.030:
            paramLines.append("%s %s %s %s" % (positionID, description, ",".join(class1Samples), ",".join(class2Samples)))

    if numHC > 0 and numHT == 0 and numHR > 0:
        addParamLine(paramLines, positionID, "HCvsHR", genotypeSampleDict["HC"], genotypeSampleDict["HR"])
    if numHC > 0 and numHT > 0 and numHR == 0:
        addParamLine(paramLines, positionID, "HCvsHT", genotypeSampleDict["HC"], genotypeSampleDict["HT"])
    if numHC == 0 and numHT > 0 and numHR > 0:
        addParamLine(paramLines, positionID, "HTvsHR", genotypeSampleDict["HT"], genotypeSampleDict["HR"])
    if numHC > 0 and numHT > 0 and numHR > 0:
        addParamLine(paramLines, positionID, "HCvsHTHR", genotypeSampleDict["HC"], genotypeSampleDict["HT"] + genotypeSampleDict["HR"])
        addParamLine(paramLines, positionID, "HCHTvsHR", genotypeSampleDict["HC"] + genotypeSampleDict["HT"], genotypeSampleDict["HR"])

    if len(paramLines) > 0 and len(paramLines) >= variantsPerTaskSeries:
        paramFileCount += 1
        outLineCount += len(paramLines)
        saveGroup(paramLines, paramFileCount)
        paramLines = []
        #if paramFileCount == 10:
        #    break

if len(paramLines) > 0:
    paramFileCount += 1
    outLineCount += len(paramLines)
    saveGroup(paramLines, paramFileCount)

taskSeriesFile.close()

print "%i total lines output to %s" % (outLineCount, taskSeriesFilePath)

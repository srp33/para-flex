import sys

inFilePath = sys.argv[1]
findValue = sys.argv[2].decode('string-escape')
replaceValue = sys.argv[3].decode('string-escape')

for line in file(inFilePath):
    print line.rstrip().replace(findValue, replaceValue)

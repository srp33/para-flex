import os, sys, glob, random

vcfFilePath = sys.argv[1]
inExprFilePath = sys.argv[2]
samplesFilePath = sys.argv[3]
genesFilePath = sys.argv[4]
dataFilePath = sys.argv[5]

for line in file(vcfFilePath):
    if line.startswith("#CHROM"):
        genotypeSamples = line.rstrip().split("\t")[9:]
        break

inExprFile = open(inExprFilePath)
genes = inExprFile.readline().rstrip().split("\t")[1:]
exprData = []
for line in inExprFile:
    lineItems = line.rstrip().split("\t")
    exprData.append(lineItems)
inExprFile.close()

exprSamples = [x[0] for x in exprData]
overlappingSamples = sorted(list(set(genotypeSamples) & set(exprSamples)))

exprData = [exprData[exprSamples.index(sample)][1:] for sample in overlappingSamples]

dataFile = open(dataFilePath, 'w')
for x in exprData:
    dataFile.write("\t".join(x) + "\n")
dataFile.close()

samplesFile = open(samplesFilePath, 'w')
samplesFile.write("\n".join(overlappingSamples) + "\n")
samplesFile.close()

genesFile = open(genesFilePath, 'w')
genesFile.write("\n".join(genes) + "\n")
genesFile.close()

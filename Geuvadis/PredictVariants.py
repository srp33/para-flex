import os, sys, glob, random, posix
from datetime import datetime
import numpy as np
from sklearn.svm import SVC
from sklearn.preprocessing import StandardScaler
from sklearn.cross_validation import *
from sklearn.metrics import roc_curve, auc, average_precision_score
from scipy import interp

paramsFilePath = os.path.expanduser(sys.argv[1])
rnaSeqDataFilePath = os.path.expanduser(sys.argv[2])
rnaSeqSamplesFilePath = os.path.expanduser(sys.argv[3])
rnaSeqGenesFilePath = os.path.expanduser(sys.argv[4])
outFilePath = os.path.expanduser(sys.argv[5])

#cOptions = [0.01, 0.1, 1.0]
cOptions = [1.0]

def printStatus(text):
    print "%s -- %s" % (text, str(datetime.now()))
    sys.stdout.flush()

def makePredictions(variantID, description, variantData, cv, targets, C):
    printStatus("Executing for %s, %s, %s, C=%.3f" % (paramsFilePath, variantID, description, C))

    mean_tpr = 0.0
    mean_fpr = np.linspace(0, 1, 100)
    precision_scores = []
    precision_recall_areas = []
    foldCount = 0
    for trainIndices, testIndices in cv:
        foldCount += 1
        trainSamples = rnaSeqSamples[trainIndices]
        testSamples = rnaSeqSamples[testIndices]

        clf = SVC(C=C, kernel='rbf', gamma=0.0, shrinking=True, probability=True, tol=0.001, cache_size=200, class_weight='auto', verbose=False, max_iter=-1, random_state=0)
        printStatus("Fitting model for fold %i" % foldCount)
        model = clf.fit(variantData[trainIndices,], targets[trainIndices])
        probs = model.predict_proba(variantData[testIndices,])[:, 1]
        scores = model.decision_function(variantData[testIndices,])

        fpr, tpr, thresholds = roc_curve(targets[testIndices], probs)
        #fpr, tpr, thresholds = roc_curve(targets[testIndices], scores)
        mean_tpr += interp(mean_fpr, fpr, tpr)
        mean_tpr[0] = 0.0
        roc_auc = auc(fpr, tpr)

        precision_scores.append(average_precision_score(targets[testIndices], probs))

    mean_tpr /= len(cv)
    mean_tpr[-1] = 1.0
    mean_auc = auc(mean_fpr, mean_tpr)

    mean_precision_score = sum(precision_scores) / float(len(precision_scores))

    return ["%.6f" % mean_auc, "%.6f" % mean_precision_score]

rnaSeqSamples = np.array([line.rstrip() for line in file(rnaSeqSamplesFilePath)])
rnaSeqGenes = [line.rstrip() for line in file(rnaSeqGenesFilePath)]

printStatus("Loading data")
data = np.loadtxt(rnaSeqDataFilePath, delimiter="\t")
printStatus("Scaling data")
data = StandardScaler().fit_transform(data)

outDirPath = os.path.dirname(outFilePath)
if not os.path.exists(outDirPath):
    posix.mkdir(outDirPath)

allOutput = ""
lineCount = 0
for line in file(paramsFilePath):
    lineCount += 1
    lineItems = line.split(" ")
    variantID = lineItems[0]
    description = lineItems[1]
    samples1 = [x.rstrip() for x in lineItems[2].split(",")]
    samples2 = [x.rstrip() for x in lineItems[3].split(",")]

    allGenotypeSamples = samples1 + samples2
    rnaSeqSampleIndices = [i for i in range(len(rnaSeqSamples)) if rnaSeqSamples[i] in allGenotypeSamples]
    variantData = data[rnaSeqSampleIndices,]

    targets = []

    for sample in rnaSeqSamples:
        if sample in samples1:
            targets.append(-1)
        if sample in samples2:
            targets.append(1)

    targets = np.array(targets)

    cv = StratifiedKFold(targets, n_folds=3, indices=True)

    output = [variantID, description]
    for C in cOptions:
        output.append(",".join(makePredictions(variantID, description, variantData, cv, targets, C)))

    print output
    print "# lines processed: %i" % lineCount
    allOutput += "\t".join(output) + "\n"

outFile = open(outFilePath, 'w')
outFile.write(allOutput)
outFile.close()

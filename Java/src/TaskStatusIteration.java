import java.util.ArrayList;
import java.util.HashSet;

public class TaskStatusIteration implements Comparable<TaskStatusIteration>
{
    public int TaskID;
    public String MachineID;
    public String ProcessID;
    public TaskStatusType StatusType;

    public TaskStatusIteration(int taskID, String machineID, String processID, TaskStatusType statusType)
    {
        TaskID = taskID;
        MachineID = machineID;
        ProcessID = processID;
        StatusType = statusType;
    }

    public int compareTo(TaskStatusIteration taskStatusIteration)
    {
        return (this.toString().compareTo(taskStatusIteration.toString()));
    }

    @Override
    public String toString()
    {
//        String formattedTime = String.valueOf(Time);

//        return ListUtilities.Join(ListUtilities.CreateStringList(TaskID, MachineID, ProcessID, StatusType, DateUtilities.ConvertToFullString(Time)), "\t");
        return ListUtilities.Join(ListUtilities.CreateStringList(TaskID, MachineID, ProcessID, StatusType), "\t");
    }

    public static ArrayList<TaskStatusType> GetUniqueTaskStatusTypes(ArrayList<TaskStatusIteration> taskStatusIterations)
    {
        HashSet<TaskStatusType> taskStatusTypes = new HashSet<TaskStatusType>();

        for (TaskStatusIteration statusIteration : taskStatusIterations)
            taskStatusTypes.add(statusIteration.StatusType);

        return new ArrayList<TaskStatusType>(ListUtilities.Sort(taskStatusTypes));
    }

    public static int CountTaskStatusTypes(ArrayList<TaskStatusIteration> taskStatusIterations, TaskStatusType statusType)
    {
        int count = 0;

        for (TaskStatusIteration statusIteration : taskStatusIterations)
            if (statusIteration.StatusType.equals(statusType))
                count += 1;

        return count;
    }
}

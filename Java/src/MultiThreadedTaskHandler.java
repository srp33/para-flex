import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/** This class encapsulates logic for executing computational tasks across one or more threads.
 */
public class MultiThreadedTaskHandler
{
    private CallableTasksList _callables;
    private String _description;
    /** Whether to log time spent on this activity */
    private boolean _logTime;
    private int _numThreads;

    /** This constructor accepts the arguments to the class that are necessary to execute tasks in parallel and initializes private variables.
     * @param callables List of callable objects to be executed
     * @param description A description of the tasks that will be executed in parallel
     * @param numThreads Number of threads on which to execute the callable objects
     * @throws Exception
     */
    public MultiThreadedTaskHandler(CallableTasksList callables, String description, boolean logTime, int numThreads)
    {
        _callables = callables;
        _description = description;
        _logTime = logTime;
        _numThreads = numThreads;
    }

    /** Iterates through a list of tasks until all have been executed successfully or until the maximum # of allowable exceptions have occurred.
     *
     * @throws Exception
     */
    public void ExecuteTasks() throws Exception
    {
        Singletons.DbQueue = new DbQueue(Singletons.TaskSeries.toString(), _description).CreateTable();

        ExecuteCallables();

        while (_callables.Tasks.size() > 0)
        {
            Pause();
            ExecuteCallables();
        }
    }

    /** Executes multiple callable objects in a multithreaded fashion
     *
     * @return Objects that are returned from each callable object
     * @throws Exception
     */
    private void ExecuteCallables() throws Exception
    {
        // Initialize the service
        ExecutorService service = new TimeoutThreadPoolExecutor(_logTime, _numThreads);

        try
        {
            Singletons.DbQueue.DeleteExpiredTasks();
            HashSet<Integer> completedTaskIDs = Singletons.DbQueue.GetCompletedTaskIDs();

            // Submit each task to a queue to be executed
            ArrayList<Future<TaskStatus>> futures = new ArrayList<Future<TaskStatus>>();
            ArrayList<Integer> callableIndicesToRemove = new ArrayList<Integer>();
            for (int i=0; i < _callables.Tasks.size(); i++)
            {
                LockedCallable lockedCallable = (LockedCallable)_callables.Tasks.get(i);

                if (completedTaskIDs.contains(lockedCallable.ID))
                    callableIndicesToRemove.add(i);
                else
                    futures.add(service.submit(lockedCallable));
            }

            Collections.sort(callableIndicesToRemove, Collections.reverseOrder());
            for (int i : callableIndicesToRemove)
                _callables.Tasks.remove(i);

            // Parse through the results of the execution
            for (int i=futures.size() - 1; i >= 0; i--)
            {
                TaskStatus status = futures.get(i).get();

                if (!UserSettings.MONITOR_FILE.equals("") && _callables.Tasks.get(i) instanceof LockedCallable)
                {
                    if (_logTime)
                        Singletons.TaskStatusIterations.add(new TaskStatusIteration(((LockedCallable)(_callables.Tasks.get(i))).ID, Singletons.MachineAddress, Singletons.ProcessID, status.StatusType));
                }

                if (status.StatusType.equals(TaskStatusType.Completed))
                    _callables.Tasks.remove(i);
            }
        }
        finally
        {
            // Very important to shut down the service
            service.shutdown();
        }
    }

    private void Pause() throws Exception
    {
        long pauseMilliseconds = UserSettings.PAUSE_SECONDS * 1000;

        if (pauseMilliseconds == 0)
            return;

        Singletons.Log.Debug("Pausing for " + UserSettings.PAUSE_SECONDS + " seconds: " + _description + ". " + _callables.Tasks.size() + " tasks remain to be executed. Other threads may be competing to process these tasks.");

        MiscUtilities.Sleep(pauseMilliseconds);

        if (!UserSettings.MONITOR_FILE.equals("") && _logTime)
            Singletons.OverallPauseMilliseconds.addAndGet(pauseMilliseconds);

        Singletons.Log.Debug("Done with pause: " + _description + ".");
    }
}

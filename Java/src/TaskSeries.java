import java.util.ArrayList;
import java.util.concurrent.Callable;

/** This class contains high-level commands for executing a task series.
 *
 * @author Stephen Piccolo
 */
public class TaskSeries
{
    /** This method orchestrates the computational tasks that will be performed, depending on what is requested by the user. It is also intended to provide a high-level view of the workflow that is followed.
     *
     * @throws Exception
     */
    public int Execute() throws Exception
    {
        int numTasks = 0;

        ArrayList<CallableTasksList> callableTaskSubsetList = ParseCallableTasksFromFile();

        for (int i = 0; i < callableTaskSubsetList.size(); i++)
        {
            CallableTasksList callableTaskSubset = callableTaskSubsetList.get(i);
            numTasks += callableTaskSubset.Tasks.size();

            MultiThreadedTaskHandler taskHandler = new MultiThreadedTaskHandler(callableTaskSubset, "TaskSubset" + (i + 1), true, UserSettings.NUM_THREADS);
            taskHandler.ExecuteTasks();
        }

        // Add reporting tasks
        CallableTasksList reportTasks = new CallableTasksList();
        reportTasks.Tasks.add(new LockedCallable(0, "Report average lock time", false, 1, new Callable<Integer>()
        {
            public Integer call() throws Exception
            {
                try
                {
                    FileUtilities.WriteTextToFile(UserSettings.REPORT_AVERAGE_LOCK_TIME_FILE, Singletons.DbQueue.ReportAverageLockTime());
                    FileUtilities.WriteLinesToFile(UserSettings.REPORT_TASKS_PER_THREAD_FILE, Singletons.DbQueue.ReportTasksPerThread());

                    return 0;
                } catch (Exception ex)
                {
                    Singletons.Log.Exception("Error when attempting to report performance metrics.", ex);
                    return 1;
                }
            }
        }));

        MultiThreadedTaskHandler taskHandler = new MultiThreadedTaskHandler(reportTasks, "ReportTasks", false, UserSettings.NUM_THREADS);
        taskHandler.ExecuteTasks();

        return numTasks;
    }

    private ArrayList<CallableTasksList> ParseCallableTasksFromFile() throws Exception
    {
        ArrayList<String> lines = FileUtilities.ReadLinesFromFile(UserSettings.TASK_SERIES_FILE, "#!");
        int numLines = lines.size();

        int lineNumber = 0;

        String commandFileHeader = "";
        if (lines.size() > 0 && lines.get(0).startsWith("#!"))
        {
            commandFileHeader = lines.get(0) + "\n";
            lines.remove(0);
            lineNumber++;
        }

        ArrayList<CallableTasksList> allCallableTaskList = new ArrayList<CallableTasksList>();
        ArrayList<ArrayList<String>> commandsList = new ArrayList<ArrayList<String>>();

        for (int i=1; i<=lines.size(); i++)
        {
            String commandText = lines.get(i - 1).trim();
            lineNumber++;

            if (commandText.length() == 0)
                continue;

            if (commandText.startsWith("#") && !commandText.startsWith("#wait"))
                continue;

            if (commandText.startsWith("#wait"))
            {
                int maxNumConcurrentTasks = Integer.MAX_VALUE;

                if (commandsList.size() > 0)
                {
                    String[] waitCommandParts = commandText.split(" ");

                    if (waitCommandParts.length > 1)
                    {
                        Singletons.Log.Debug("Wait command parts:");
                        Singletons.Log.Debug(ListUtilities.Join(waitCommandParts, " "));

                        if (DataTypeUtilities.IsInteger(waitCommandParts[1]))
                            maxNumConcurrentTasks = Integer.parseInt(waitCommandParts[1]);
                        else
                            Singletons.Log.ExceptionFatal("The max number of concurrent tasks [" + waitCommandParts[1] + "] indicated on line " + lineNumber + " is invalid.");
                    }

                    allCallableTaskList.add(BuildCallablesFromCommands(commandsList, maxNumConcurrentTasks));
                    commandsList = new ArrayList<ArrayList<String>>();
                }
            }
            else
            {
                commandsList.add(ListUtilities.CreateStringList(lineNumber, "Line " + lineNumber + " / " + numLines +  " - " + commandText, true, commandFileHeader + commandText));
            }
        }

        if (commandsList.size() > 0)
            allCallableTaskList.add(BuildCallablesFromCommands(commandsList, Integer.MAX_VALUE));

        return allCallableTaskList;
    }

    private CallableTasksList BuildCallablesFromCommands(ArrayList<ArrayList<String>> commandsList, int maxNumConcurrentTasks) throws Exception
    {
        CallableTasksList callableTaskSubset = new CallableTasksList();

        for (ArrayList<String> commands : commandsList)
            callableTaskSubset.Tasks.add(new LockedCallable(Integer.parseInt(commands.get(0)), commands.get(1), Boolean.parseBoolean(commands.get(2)), maxNumConcurrentTasks, CreateCallableFromCommandText(commands.get(3))));

        return callableTaskSubset;
    }

    private Callable<Integer> CreateCallableFromCommandText(final String commandText) throws Exception
    {
        return new Callable<Integer>()
        {
            public Integer call() throws Exception
            {
                return CommandLineClient.RunCommand(commandText, true);
            }
        };
    }

    @Override
    public String toString()
    {
        return new java.io.File(UserSettings.TASK_SERIES_FILE).getName().replace(".", "_");
    }
}
import java.io.*;

/** This class is used when it is necessary to search the file system using wildcard file names.
 * @author Stephen Piccolo
 */
public class WildCardFilenameFilter implements FilenameFilter
{
    private WildCardFileFilter _filter;

    /** Basic constructor
     *
     * @param pattern File name pattern (can include wildcard (*) characters)
     */
    public WildCardFilenameFilter(String pattern)
    {
        _filter = new WildCardFileFilter(pattern);
    }

    public boolean accept(File dir, String fileName)
    {
        File file = new File(dir.getAbsolutePath() + "/" + fileName);
        return _filter.accept(file);
    }
}

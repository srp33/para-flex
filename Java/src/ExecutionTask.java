public class ExecutionTask
{
    public String Description;
    public String Command;

    public ExecutionTask(String description, String command)
    {
        Description = description;
        Command = command;
    }
}

import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/** This class is used to monitor threads that are executed in parallel. If a thread executes longer than a configurable timeout period, execution is terminated. */
public class TimeoutThreadPoolExecutor extends ThreadPoolExecutor
{
    /** Whether to log time spent on this activity */
    private final boolean _logTime;
    private final HashMap<String, Timer> _timerMap = new HashMap<String, Timer>();
    private final HashMap<String, java.util.Date> _executionTimeMap = new HashMap<String, java.util.Date>();

    /** Constructor
     *
     * @param numberThreads The maximum number of threads that will be executed
     */
    public TimeoutThreadPoolExecutor(boolean logTime, int numberThreads)
    {
        super(numberThreads, numberThreads, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
        _logTime = logTime;
    }

    @Override
    protected void beforeExecute(final Thread thread, final Runnable runnable)
    {
        if (_logTime)
            Singletons.NumThreadExecutions.addAndGet(1);

        if (!UserSettings.MONITOR_FILE.equals(""))
            _executionTimeMap.put(runnable.toString(), DateUtilities.GetCurrentDate());

        super.beforeExecute(thread, runnable);

        // Start the timing and set the timeout period
        Timer timer = new Timer(true);
        //timer.schedule(new TimeOutTask(thread), UserSettings.THREAD_TIMEOUT_MINUTES * 60000L);
        timer.schedule(new TimeOutTask(thread), UserSettings.THREAD_TIMEOUT_SECONDS * 1000L);

        // Store the timer in a global variable so it can be accessed later
        _timerMap.put(runnable.toString(), timer);
    }

    @Override
    protected void afterExecute(Runnable runnable, Throwable throwable)
    {
        try
        {
            // Cancel the timer if it is still there
            if (runnable != null && _timerMap.containsKey(runnable.toString()))
            {
                Timer timer = _timerMap.get(runnable.toString());

                if (timer != null)
                    timer.cancel();
            }
        }
        catch (Exception ex)
        {
            Singletons.Log.ExceptionNoProblem("Error occurred when attempting to cancel timed-out thread.", ex);
        }
        finally
        {
            // Clean up
            if (runnable != null && _timerMap.containsKey(runnable.toString()))
                _timerMap.remove(runnable.toString());

            super.afterExecute(runnable, throwable);
        }

        if (!UserSettings.MONITOR_FILE.equals("") && _executionTimeMap.containsKey(runnable.toString()))
        {
            Date start = _executionTimeMap.get(runnable.toString());
            _executionTimeMap.remove(runnable.toString());

            if (start != null && _logTime)
                Singletons.OverallExecutionMilliseconds.addAndGet((long)DateUtilities.DifferenceInMilliseconds(DateUtilities.GetCurrentDate(), start));
        }
    }

    /** A simple task that enables tasks to be interrupted. */
    private class TimeOutTask extends TimerTask
    {
        Thread _thread;

        TimeOutTask(Thread thread)
        {
            _thread = thread;
        }

        public void run()
        {
            if(_thread != null && _thread.isAlive())
                _thread.interrupt();
        }
    }
}

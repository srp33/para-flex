/** This class stores application wide values that may be specified by the user.
 * @author Stephen Piccolo
 */
public class UserSettings
{
    /** Location of the queue */
    public static String QUEUE_LOCATION;

    /** Path to the file that contains commands to execute */
    public static String TASK_SERIES_FILE;

    /** The maximum number of threads that will be used per node */
    public static int NUM_THREADS;

    /** The maximum length of time that a thread will run before timing out */
    public static long THREAD_TIMEOUT_SECONDS;

    /** The length of time that a thread will pause before retrying to execute a task */
    public static long PAUSE_SECONDS;

    /** Whether debugging should be turned on */
    public static boolean DEBUG;

    public static String MONITOR_FILE;

    public static String REPORT_AVERAGE_LOCK_TIME_FILE;

    public static String REPORT_TASKS_PER_THREAD_FILE;
}

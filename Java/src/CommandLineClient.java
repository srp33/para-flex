import java.io.BufferedReader;
import java.io.InputStreamReader;

/** This class is used to execute commands at the command line. It contains functionality to invoke commands.
 * @author Stephen Piccolo
 */
public class CommandLineClient
{
    /** Executes the specified command at the command line
     *
     * @param commandText This is the command that will executed
     * @throws Exception
     */
    public static Integer RunCommand(String commandText, boolean logTime) throws Exception
    {
        // Create temp file with the command text
        String tempCommandFilePath = ApplicationSettings.TEMP_DIR + MiscUtilities.GetUniqueID();
        Singletons.Log.Debug(commandText);
        FileUtilities.WriteTextToFile(tempCommandFilePath, commandText);
        new java.io.File(tempCommandFilePath).setExecutable(true);

        // Start up an external process
        ProcessBuilder processBuilder = new ProcessBuilder(tempCommandFilePath);

        if (logTime)
            Singletons.NumCommandExecutions.addAndGet(1);

        // Mark the time before execution starts
        java.util.Date before = DateUtilities.GetCurrentDate();

        int exitValue = 1;

        Process p = processBuilder.start();

        try
        {
            // Read the output and error streams from the process
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

            // Parse the standard output stream
            String s;
            while ((s = stdInput.readLine()) != null)
                Singletons.Log.Info(commandText + " --> " + s);

            // Parse the standard error stream
            while ((s = stdError.readLine()) != null)
                Singletons.Log.Debug(commandText + " --> STDERR: " + s);

            // Close the process objects
            stdInput.close();
            stdError.close();

            if (logTime)
            // Log how long it took to execute
                Singletons.CommandExecutionMilliseconds.addAndGet((long) DateUtilities.DifferenceInMilliseconds(DateUtilities.GetCurrentDate(), before));

            try
            {
                exitValue = p.waitFor();
            }
            catch (InterruptedException intEx)
            {
                Singletons.Log.Debug("Timeout interrupt for:" + commandText);
                exitValue = 333;
            }
        }
        catch (Exception ex)
        {
            Singletons.Log.Exception("Exception when trying to execute subprocess.", ex);
        }
        finally
        {
            try
            {
                // Clean up
                p.destroy();
                FileUtilities.DeleteFile(tempCommandFilePath);
            }
            catch (Exception ex)
            {
                Singletons.Log.Exception("Exception when trying to clean up after executing subprocess.", ex);
            }
        }

        return exitValue;
    }
}

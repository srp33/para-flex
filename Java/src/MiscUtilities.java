import java.net.InetAddress;
import java.util.UUID;

/** This class contains general-purpose helper methods that are used in various places throughout the code. It also contains Singleton objects (those that are instantiated only once and stored as static variables).
 * @author Stephen
 */
public class MiscUtilities
{
    /** Indicates the IP address of the machine where this software is being run
     *
     * @return IP address
     * @throws Exception
     */
    public static String GetMachineAddress() throws Exception
    {
        return InetAddress.getLocalHost().getHostAddress();
    }

    public static String GetProcessID()
    {
        String processID = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();

        if (processID.contains("@"))
            processID = processID.substring(0, processID.indexOf("@"));

        return processID;
    }

    /** Generates a unique identifier randomly
     *
     * @return Random unique identifier
     */
    public static String GetUniqueID()
    {
        return "id." + UUID.randomUUID();
    }

    public static String GetThreadName() throws Exception
    {
        return Thread.currentThread().getName();
    }

    /** Deletes all files and directories recursively from a "core" directory...a directory that contains key files that are created in the process of executing tasks.
     *
     * @param directoryPath Absolute path to the directory
     * @throws Exception
     */
    public static void DeleteCoreDirectory(String directoryPath) throws Exception
    {
        FileUtilities.DeleteAllFilesAndDirectoriesRecursively(directoryPath);
        FileUtilities.DeleteDirectory(directoryPath);
    }

    /** Causes the current thread to sleep for a certain length of time.
     *
     * @param milliseconds The number of milliseconds that the thread should sleep
     * @throws Exception
     */
    public static void Sleep(long milliseconds) throws Exception
    {
        Thread.currentThread().sleep(milliseconds);
    }

    /** If you have a string that has words that are each capitalized but are scrunched together without spaces between them, this method adds a space between each word.
     *
     * @param text Text that may need to be separated
     * @return Modified text
     */
    public static String SeparateWords(String text)
    {
        String modString = "";

        for (int i=0; i<text.length(); i++)
        {
            // Look for an upper-case letter
            if (i > 0 && Character.isUpperCase(text.charAt(i)))
                modString += " ";
            modString += text.charAt(i);
        }

        return modString;
    }

//    public static void UpdateOverallExecutionTime(boolean addTime, boolean updateCheckpoint)
//    {
//        if (UserSettings.MONITOR_FILE.equals(""))
//            return;
//
//        Date now = DateUtilities.GetCurrentDate();
//
//        if (addTime)
//            Singletons.OverallExecutionMilliseconds.addAndGet((long)DateUtilities.DifferenceInMilliseconds(now, Singletons.LatestExecutionTimeCheckpoint));
//
//        if (updateCheckpoint)
//            Singletons.LatestExecutionTimeCheckpoint = now;
//    }
}
public enum TaskStatusType
{
    PreviouslyCompleted,
    Locked,
    Completed,
    UpdateLockedStatusFailed,
    UpdateCompletedStatusFailed,
    FailedExecution
}

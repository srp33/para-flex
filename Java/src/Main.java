import java.util.Collections;
import java.util.HashMap;

/** This is the class that gets invoked when the program begins to execute.
 * @author Stephen Piccolo
 */
public class Main
{
    /** This method is the first one invoked when the program is run from the command line. It sets up a parallel task series, processes it, and handles exceptions at a high level.
     *
     * @param args Array of arguments that are passed to this application from the Java runtime
     */
    public static void main(String[] args)
    {
        try
        {
            Singletons.MachineAddress = MiscUtilities.GetMachineAddress();
            Singletons.ProcessID = MiscUtilities.GetProcessID();
            Singletons.OverallStartTime = DateUtilities.GetCurrentDate();

            Singletons.Log = new Log();
            ParseCoreCommandLineSettings(args);

            ParseTaskExecutionCommandLineSettings(args);
            int totalNumberCommands = Singletons.TaskSeries.Execute();

            Singletons.OverallStopTime = DateUtilities.GetCurrentDate();

            CreateReport(totalNumberCommands);

            Singletons.Log.Info("Successfully completed!");
        }
        catch (Exception ex)
        {
            Singletons.Log.Exception("", ex);
            System.exit(1); // Not sure if this is necessary, but keeping it just in case
        }
    }

    /** Parses configuration settings that have been specified at the command line and saves these settings so they can be used throughout the application.
     *
     * @param args Command-line arguments
     * @throws Exception
     */
    private static void ParseCoreCommandLineSettings(String[] args) throws Exception
    {
        ApplicationSettings.MAIN_DIR = System.getProperty("user.dir") + "/";
        ApplicationSettings.INTERNALS_DIR = InitializeDirectory(ApplicationSettings.MAIN_DIR, "Internals");
        ApplicationSettings.TEMP_DIR = InitializeDirectory(ApplicationSettings.INTERNALS_DIR, "Temp/" + MiscUtilities.GetUniqueID());

        UserSettings.DEBUG = Boolean.parseBoolean(GetArgValue(args, "DEBUG", "true"));
        UserSettings.QUEUE_LOCATION = GetArgValue(args, "QUEUE_LOCATION", ApplicationSettings.INTERNALS_DIR);

        // Is it a file-based queue?
        if (FileUtilities.DirectoryExists(UserSettings.QUEUE_LOCATION))
        {
            UserSettings.QUEUE_LOCATION += "Queue/" + ApplicationSettings.TASK_SERIES_DESCRIPTION + "/";
            FileUtilities.CreateDirectoryIfNotExists(UserSettings.QUEUE_LOCATION);
            ApplicationSettings.HAS_FILE_QUEUE = true;
        }
        else
        {
            ApplicationSettings.HAS_FILE_QUEUE = false;
        }
    }

    /** Parses configuration settings that have been specified at the command line and saves these settings so they can be used throughout the application.
     *
     * @param args Command-line arguments
     * @throws Exception
     */
    private static void ParseTaskExecutionCommandLineSettings(String[] args) throws Exception
    {
        UserSettings.TASK_SERIES_FILE = GetArgValue(args, "TASK_SERIES_FILE", null);

        // Make sure we can find the task series file
        if (!FileUtilities.FileExists(UserSettings.TASK_SERIES_FILE))
            Singletons.Log.ExceptionFatal("Invalid task-series file: " + UserSettings.TASK_SERIES_FILE);

        // Initialize singleton object for task series
        Singletons.TaskSeries = new TaskSeries();

        ApplicationSettings.TASK_SERIES_DESCRIPTION = new java.io.File(UserSettings.TASK_SERIES_FILE).getName();

        // Parse other user settings
        UserSettings.MONITOR_FILE = GetArgValue(args, "MONITOR_FILE", "");
        UserSettings.REPORT_AVERAGE_LOCK_TIME_FILE = GetArgValue(args, "REPORT_AVERAGE_LOCK_TIME_FILE", "Report_Average_Lock_Time.txt");
        UserSettings.REPORT_TASKS_PER_THREAD_FILE = GetArgValue(args, "REPORT_TASKS_PER_THREAD_FILE", "Report_Tasks_Per_Thread.txt");
        UserSettings.NUM_THREADS = Integer.parseInt(GetArgValue(args, "NUM_THREADS", String.valueOf(Runtime.getRuntime().availableProcessors())));
        UserSettings.THREAD_TIMEOUT_SECONDS = Long.parseLong(GetArgValue(args, "THREAD_TIMEOUT_SECONDS", "3600"));
        UserSettings.PAUSE_SECONDS = Long.parseLong(GetArgValue(args, "PAUSE_SECONDS", "10"));
    }

    /** Formulates a directory path that is specific to a given purpose and ensures the directory is created.
     *
     * @param rootDirectory Root directory path
     * @param directoryName Relative directory name
     * @return Absolute directory path
     * @throws Exception
     */
    private static String InitializeDirectory(String rootDirectory, String directoryName) throws  Exception
    {
        String directoryPath = rootDirectory + directoryName + "/";

        try
        {
            return FileUtilities.CreateDirectoryIfNotExists(directoryPath);
        }
        catch (Exception ex)
        {
            Singletons.Log.ExceptionNoProblem("Error occurred when attempting to create a directory at " + directoryPath + ".", ex);
            return directoryPath;
        }
    }

    /** Parses a value with a specified key from the command-line arguments.
     *
     * @param args Command-line arguments
     * @param key Key of the argument
     * @param defaultValue Value that is used if no value is specified
     * @return Value of the argument
     * @throws Exception
     */
    private static String GetArgValue(String[] args, String key, String defaultValue) throws Exception
    {
        HashMap<String, String> keyValueMap = new HashMap<String, String>();

        for (String arg : args)
            if (arg.contains("="))
            {
                String[] parts = arg.split("=");
                if (parts.length == 2 && parts[0].length() > 0 && parts[1].length() > 0)
                    keyValueMap.put(parts[0], parts[1]);
            }

        if (keyValueMap.containsKey(key))
            return keyValueMap.get(key);
        else
        {
            if (defaultValue == null)
                throw new Exception("A value for " + key + " must be set at the command line.");
            else
                return defaultValue;
        }
    }

    private static void CreateReport(int numCommands)
    {
        try
        {
            double elapsedSeconds = DateUtilities.DifferenceInMilliseconds(Singletons.OverallStopTime, Singletons.OverallStartTime) / 1000.0;

            String performanceDescription = "Machine address\t" + Singletons.MachineAddress + "\n";
            performanceDescription += "Process ID\t" + Singletons.ProcessID + "\n";
            performanceDescription += "Total commands\t" + numCommands + "\n";
            performanceDescription += "Command executions\t" + Singletons.NumCommandExecutions + "\n";
            performanceDescription += "Thread executions\t" + Singletons.NumThreadExecutions + "\n";
            performanceDescription += "Attempted commands\t" + Singletons.NumLockedCommandsAttempted + "\n";
            performanceDescription += "Completed commands\t" + Singletons.NumLockedCommandsCompleted + "\n";
            performanceDescription += "Number exceptions occurred\t" + Singletons.NumExceptionsOccurred + "\n";
            performanceDescription += "Application elapsed time (s)\t" + elapsedSeconds + "\n";
            performanceDescription += "Overall execution time (s)\t" + Singletons.OverallExecutionMilliseconds.floatValue() / 1000.0 + "\n";
            performanceDescription += "Overall execution time per command (s)\t" + Singletons.OverallExecutionMilliseconds.floatValue() / 1000.0 / (float)numCommands + "\n";
            performanceDescription += "Command execution time (s)\t" + Singletons.CommandExecutionMilliseconds.floatValue() / 1000.0 + "\n";
            performanceDescription += "Command execution time per command (s)\t" + Singletons.CommandExecutionMilliseconds.floatValue() / 1000.0 / (float)numCommands + "\n";
            performanceDescription += "Pause time (s)\t" + Singletons.OverallPauseMilliseconds.floatValue() / 1000.0 + "\n";
            performanceDescription += "Pause time per command (s)\t" + Singletons.OverallPauseMilliseconds.floatValue() / 1000.0 / (float)numCommands + "\n";

            performanceDescription += "Status type frequencies:\n";
            for (TaskStatusType statusType : TaskStatusIteration.GetUniqueTaskStatusTypes(Singletons.TaskStatusIterations))
                performanceDescription += statusType.name() + "\t" + String.valueOf(TaskStatusIteration.CountTaskStatusTypes(Singletons.TaskStatusIterations, statusType)) + "\n";

            Singletons.Log.Info(performanceDescription);

            if (!UserSettings.MONITOR_FILE.equals(""))
                FileUtilities.WriteLineToFile(UserSettings.MONITOR_FILE, performanceDescription);

            if (UserSettings.DEBUG)
            {
                String statusSummary = "";

                Collections.sort(Singletons.TaskStatusIterations);
                for (TaskStatusIteration taskStatusIteration : Singletons.TaskStatusIterations)
                    statusSummary += taskStatusIteration.toString() + "\n";

                Singletons.Log.Info(statusSummary);
            }
        }
        catch (Exception ex)
        {
            Singletons.Log.ExceptionFatal("Error occurred when attempting to generate a report.", ex);
        }
    }
}
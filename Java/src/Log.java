import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

/** This class contains methods for logging information about the program's execution. Values are output to the screen and to output files.
 * @author Stephen Piccolo
 */
public class Log
{
    private final static String HORIZONTAL_LINE = "*************************************************";

    /** Saves debug information
     *
     * @param text Debug text
     */
    public void Debug(Object text)
    {
        if (UserSettings.DEBUG)
            Print(FormatText(text), true);
    }

    /** Saves logging information
     *
     * @param text Logging text
     */
    public void Info(Object text)
    {
        Print(FormatText(text), false);
    }

    public void ExceptionNoProblem(String message)
    {
        ExceptionNoProblem("", new Exception(message));
    }

    /** Saves debugging information for a non-fatal error.
     *
     * @param ex Exception that occurred.
     */
    public void ExceptionNoProblem(String message, Throwable ex)
    {
        if (!UserSettings.DEBUG)
            return;

        Print(HORIZONTAL_LINE, true);
        Print("A non-fatal error occurred. It will be logged but should not affect processing of this program.", true);
        if (message != "")
            Print(message, true);
        Print(GetStackTrace(ex), true);
        Print(HORIZONTAL_LINE, true);
    }

    /** Saves exception information
     *
     * @param ex Exception object
     */
    public void Exception(String message, Throwable ex)
    {
        Singletons.NumExceptionsOccurred.addAndGet(1);

        Print(HORIZONTAL_LINE, true);

        if (Singletons.NumExceptionsOccurred.intValue() >= 25)
            Print("More than " + Singletons.NumExceptionsOccurred.intValue() + " non-fatal exceptions have occurred, so this process will terminate now.", true);
        else
            Print("An error occurred. An attempt will be made to recover from this error.", true);

        if (message != "")
            Print(message, true);

        Print(GetStackTrace(ex), true);
        Print(HORIZONTAL_LINE, true);

        if (Singletons.NumExceptionsOccurred.intValue() >= 25)
            System.exit(1);
     }

    public void ExceptionFatal(String message)
    {
        ExceptionFatal("", new Exception(message));
    }

    /** Saves exception information when the exception is severe enough that execution of the program should be halted.
     *
     * @param ex Exception object
     */
    public void ExceptionFatal(String message, Throwable ex)
    {
        Print(HORIZONTAL_LINE, true);
        Print("A fatal error occurred. This process will terminate now.", true);
        if (message != "")
            Print(message, true);
        Print(GetStackTrace(ex), true);
        Print(HORIZONTAL_LINE, true);

        System.exit(1);
     }

    /** Obtains stack-trace information when an exception has occurred.
     *
     * @param throwable Exception object
     * @return Stack-trace information
     */
    public String GetStackTrace(Throwable throwable)
    {
        if (throwable == null)
            return "<null exception>";

        Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        throwable.printStackTrace(printWriter);
        return result.toString();
    }

    private String FormatText(Object text)
    {
        return Singletons.MachineAddress + " | " + Singletons.ProcessID + " | " + ApplicationSettings.TASK_SERIES_DESCRIPTION + " | " + String.valueOf(text);
    }

    private static void Print(Object x, boolean toStdErr)
    {
        String out = x == null ? "<null>" : String.valueOf(x);

        if (out.equals(""))
            return;

        if (toStdErr)
            System.err.println(out);
        else
            System.out.println(out);
    }
}

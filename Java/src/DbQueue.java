import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;

public class DbQueue
{
    private static String DbDriver = "org.h2.Driver";
    private static String CreateTableSql = "CREATE TABLE IF NOT EXISTS %s(TaskID INT(32), TaskSubset VARCHAR(31), MachineAddress VARCHAR(31) NOT NULL, ProcessID VARCHAR(31) NOT NULL, ThreadID VARCHAR(31) NOT NULL, Status BIT, WhenLocked DATETIME, WhenCompleted DATETIME, PRIMARY KEY (TaskID))";
    private static String GetTasksCompleteSql = "SELECT TaskID FROM %s WHERE Status = 1";
    //private static String LockTaskSql = "INSERT INTO %s (SELECT %d as TASKID, '%s' AS TaskSubset, '%s' AS MACHINEADDRESS, '%s' AS PROCESSID, '%s' AS THREADID, 0 AS STATUS, CURRENT_TIMESTAMP() AS WhenLocked, NULL AS WhenCompleted FROM (SELECT COUNT(TaskID) FROM %s) WHERE %d NOT IN (SELECT TASKID FROM %s) AND IFNULL((SELECT COUNT(TaskID) FROM %s WHERE Status = 0 and TaskSubset = '%s' GROUP BY TaskSubset), 0) < %d)";
    private static String LockTaskSql = "INSERT INTO %s SELECT %d as TASKID, '%s' AS TaskSubset, '%s' AS MACHINEADDRESS, '%s' AS PROCESSID, '%s' AS THREADID, 0 AS STATUS, CURRENT_TIMESTAMP() AS WhenLocked, NULL AS WhenCompleted FROM (SELECT %d AS TaskID FROM (SELECT COUNT(1) FROM %s)) a LEFT JOIN %s b ON a.TaskID = b.TaskID WHERE b.TaskID IS NULL";
    private static String LockTaskSql_MaxConcurrent = " AND IFNULL((SELECT COUNT(TaskID) FROM %s WHERE Status = 0 and TaskSubset = '%s' GROUP BY TaskSubset), 0) < %d";
    private static String CompleteTaskSql = "UPDATE %s SET Status = 1, WhenCompleted = CURRENT_TIMESTAMP() WHERE TaskID = %d";
    private static String DeleteTaskSql = "DELETE FROM %s WHERE TaskID = %d";
    private static String DeleteExpiredTasksSql = "DELETE FROM %s WHERE Status = 0 AND DATEDIFF('SECOND', WhenLocked, CURRENT_TIMESTAMP()) > %d";
    private static String ReportAverageLockTimeSql = "SELECT AVG(DATEDIFF('MILLISECOND', WhenLocked, WhenCompleted)) FROM %s WHERE WhenCompleted IS NOT NULL";
    private static String ReportTasksPerThreadSql = "SELECT COUNT(TaskID), TaskSubset, MachineAddress, ProcessID, ThreadID FROM %s WHERE WhenCompleted IS NOT NULL GROUP BY TaskSubset, MachineAddress, ProcessID, ThreadID";

    private String _tableName;
    private String _taskSubset;
    private Connection _conn = null;

    public DbQueue(String tableName, String taskSubset)
    {
        _tableName = tableName;
        _taskSubset = taskSubset;
    }

    private static String GetConnectionString()
    {
        return "jdbc:h2:" + UserSettings.QUEUE_LOCATION + ";DB_CLOSE_ON_EXIT=TRUE";
    }

    private Connection OpenConnection() throws Exception
    {
        if (_conn == null)
        {
            Class.forName(DbQueue.DbDriver);
            _conn = DriverManager.getConnection(GetConnectionString(), "sa", "");
        }

        return _conn;
    }

    private Object ExecuteQueryScalar(String sql) throws Exception
    {
        Singletons.Log.Debug(sql);

        Statement preparedStatement = OpenConnection().createStatement();
        ResultSet rs = preparedStatement.executeQuery(sql);

        boolean next = rs.next();
        Object scalar = rs.getObject(1);

        rs.close();

        return scalar;
    }

    private ArrayList<ArrayList<String>> ExecuteQueryArrayList(String sql) throws Exception
    {
        Singletons.Log.Debug(sql);

        Statement preparedStatement = OpenConnection().createStatement();
        ResultSet rs = preparedStatement.executeQuery(sql);
        int columnCount = rs.getMetaData().getColumnCount();

        ArrayList<ArrayList<String>> results = new ArrayList<ArrayList<String>>();

        ArrayList<String> columnNames = new ArrayList<String>();
        for (int i=0; i<columnCount; i++)
            columnNames.add(rs.getMetaData().getColumnName(i+1));
        results.add(columnNames);

        while (rs.next())
        {
            ArrayList<String> result = new ArrayList<String>();
            for (int i=0; i<columnCount; i++)
                result.add(String.valueOf(rs.getObject(i + 1)));

            results.add(result);
        }

        rs.close();

        return results;
    }

    public DbQueue CreateTable() throws Exception
    {
        ModifyTask(String.format(CreateTableSql, _tableName));
        return this;
    }

    private int ModifyTask(String sql) throws Exception
    {
        try
        {
            Singletons.Log.Debug(sql);

            Statement statement = OpenConnection().createStatement();
            int numRowsAffected = statement.executeUpdate(sql);
            statement.close();

            return numRowsAffected;
        }
        catch (Exception ex)
        {
            Singletons.Log.Exception("Error occurred when attempting to modify task.", ex);
            return -1;
        }
    }

    public boolean LockTask(int taskID, int maxNumberConcurrentTasks) throws Exception
    {
        String sql = String.format(LockTaskSql, _tableName, taskID, _taskSubset, Singletons.MachineAddress, Singletons.ProcessID, MiscUtilities.GetThreadName(), taskID, _tableName, _tableName);

        if (maxNumberConcurrentTasks < Integer.MAX_VALUE)
            sql += String.format(LockTaskSql_MaxConcurrent, _tableName, _taskSubset, maxNumberConcurrentTasks);

        int numRowsAffected = ModifyTask(sql);

        return numRowsAffected == 1;
    }

    public boolean CompleteTask(int taskID) throws Exception
    {
        return ModifyTask(String.format(CompleteTaskSql, _tableName, taskID)) > 0;
    }

    public HashSet<Integer> GetCompletedTaskIDs() throws Exception
    {
        String sql = String.format(GetTasksCompleteSql, _tableName);
        Singletons.Log.Debug(sql);

        Statement preparedStatement = OpenConnection().createStatement();
        ResultSet rs = preparedStatement.executeQuery(sql);

        HashSet<Integer> taskIDs = new HashSet<Integer>();

        while (rs.next())
            taskIDs.add(rs.getInt(1));

        rs.close();

        return taskIDs;
    }

    public boolean DeleteTask(int taskID) throws Exception
    {
        return ModifyTask(String.format(DeleteTaskSql, _tableName, taskID)) > 0;
    }

    public int DeleteExpiredTasks() throws Exception
    {
        int numExpiredTasksDeleted = ModifyTask(String.format(DeleteExpiredTasksSql, _tableName, UserSettings.THREAD_TIMEOUT_SECONDS));

        if (numExpiredTasksDeleted > 0)
            Singletons.Log.Debug(numExpiredTasksDeleted + " tasks deleted because they were expired.");

        return numExpiredTasksDeleted;
    }

    public String ReportAverageLockTime() throws Exception
    {
        long milliseconds = (Long)ExecuteQueryScalar(String.format(ReportAverageLockTimeSql, _tableName));
        double seconds = (double)milliseconds / 1000.0;

        return String.valueOf(seconds);
    }

    public ArrayList<ArrayList<String>> ReportTasksPerThread() throws Exception
    {
        return ExecuteQueryArrayList(String.format(ReportTasksPerThreadSql, _tableName));
    }
}

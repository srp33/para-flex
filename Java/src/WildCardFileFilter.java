import java.io.*;
import java.util.regex.*;

/** This class is used when it is necessary to search the file system using wildcard file names.
 * @author Stephen Piccolo
 */
public class WildCardFileFilter implements FileFilter
{
    private String _pattern;

     /** Basic constructor
     *
     * @param pattern File name pattern (can include wildcard (*) characters)
     */
    public WildCardFileFilter(String pattern)
    {
        _pattern = pattern.replace("*", ".*").replace("?", ".");
    }

    public boolean accept(File file)
    {
        return Pattern.compile(_pattern).matcher(file.getName()).find();
    }
}

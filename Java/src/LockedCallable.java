import java.util.concurrent.Callable;

//http://sanchit6.wordpress.com/2009/08/19/file-semaphore-mutex/

/** This class encapsulates objects necessary to run tasks in parallel across multiple threads/nodes. It contains logic for saving/deleting lock files and for handling errors that may occur. It also contains logic to check whether a status file has been created, which would indicate that this task has already been completed.
 */
public class LockedCallable implements Callable<TaskStatus>
{
    /** Unique identifier of the task */
    public int ID;
    /** A description of the task to be executed. */
    private String _description;
    /** Whether to log time spent on this activity */
    private boolean _logTime;
    /** Max number of concurrent tasks */
    private int _maxNumberConcurrentTasks;
    /** This is the callable that will actually be executed after locking has occurred. */
    private Callable<Integer> _callable;

    /** Constructor that accepts the objects that are necessary to support the function of this class.
     * @param id Unique ID of the task
     * @param displayDescription Description fo the task being executed
     * @param callable Object that will be executed when locking is successful
     */
    public LockedCallable(int id, String displayDescription, boolean logTime, int maxNumberConcurrentTasks, Callable<Integer> callable)
    {
        ID = id;
        _description = displayDescription;
        _logTime = logTime;
        _maxNumberConcurrentTasks = maxNumberConcurrentTasks;
        _callable = callable;
    }

    /** This method attempts to create a lock file that will indicate to other threads or compute nodes that the callable task is being executed. If the file cannot be created (most likely because the task is already being executed by another thread/node, then nothing will happen.
     *
     * @return Result of callable
     * @throws Exception
     */
    public TaskStatus call() throws Exception
    {
        if (!Singletons.DbQueue.LockTask(ID, _maxNumberConcurrentTasks))
            return new TaskStatus(TaskStatusType.UpdateLockedStatusFailed);

        try
        {
            if (_logTime)
                Singletons.NumLockedCommandsAttempted.addAndGet(1);

            Singletons.Log.Info("Attempt: " + _description);

            // Try to invoke the command
            Integer exitValue = _callable.call();

            if (exitValue > 0)
                return DeleteTask();

            if (!Singletons.DbQueue.CompleteTask(ID))
                return new TaskStatus(TaskStatusType.UpdateCompletedStatusFailed);

            Singletons.Log.Info("Success: " + _description);

            if (_logTime)
                Singletons.NumLockedCommandsCompleted.addAndGet(1);

            return new TaskStatus(TaskStatusType.Completed);
        }
        catch (Exception ex)
        {
            // Some exception occurred while executing the task, so abort the task
            Singletons.Log.Exception("Exception when attempting to execute: " + _description + ".", ex);

            return DeleteTask();
        }
    }

    private TaskStatus DeleteTask()
    {
        try
        {
            Singletons.DbQueue.DeleteTask(ID);
        }
        catch (Exception ex)
        {
            Singletons.Log.Exception("Exception occurred when attempting to delete failed task.", ex);
        }

        return new TaskStatus(TaskStatusType.FailedExecution);
    }
}

/** This class stores settings that are used throughout the application. Some of them are specified indirectly by the user.
 * @author Stephen Piccolo
 */
public class ApplicationSettings
{
    /** Path to the directory where this program is executing. */
    public static String MAIN_DIR;

    /** Path to the directory that contains files that are semi-hidden from the end user */
    public static String INTERNALS_DIR;

    /** Location where temporary files can be stored. */
    public static String TEMP_DIR;

    /** Indicates whether the queue is file based. If not, it uses a queue service. */
    public static boolean HAS_FILE_QUEUE;

    /** A short description of the task series. */
    public static String TASK_SERIES_DESCRIPTION;
}

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/** The objects specified in this class are instantiated only once, at the beginning of an execution and can be accessed globally.
 */
public class Singletons
{
    public static String MachineAddress;
    public static String ProcessID;

    /* Time when the application started and stopped executing */
    public static Date OverallStartTime;
    public static Date OverallStopTime;

    /* How long the application has had to pause while waiting for other threads to process */
    public static AtomicLong OverallPauseMilliseconds = new AtomicLong(0);

    public static AtomicLong CommandExecutionMilliseconds = new AtomicLong(0);

    public static AtomicLong OverallExecutionMilliseconds = new AtomicLong(0);

    public static AtomicLong NumLockedCommandsAttempted = new AtomicLong(0);

    public static AtomicLong NumLockedCommandsCompleted = new AtomicLong(0);

    public static AtomicInteger NumExceptionsOccurred = new AtomicInteger(0);

    public static AtomicLong NumCommandExecutions = new AtomicLong(0);

    public static AtomicLong NumThreadExecutions = new AtomicLong(0);

    /** An object that contains a series of tasks that will be attempted to be executed */
    public static TaskSeries TaskSeries;

    /** Object used for logging */
    public static Log Log;

    public static ArrayList<TaskStatusIteration> TaskStatusIterations = new ArrayList<TaskStatusIteration>();

    public static DbQueue DbQueue;
}

import java.util.ArrayList;

/** This class contains logic to delete any files that need to be deleted before or after the main tasks are executed. When executed with multiple threads, this is much faster than doing it at the command line. */
public class Vacuum
{
    /** This method cleans up files that can be deleted.
     *
     * @throws Exception
     */
    public void Clean(ArrayList<String> cleanupDirs) throws Exception
    {
        // Create a list of tasks that represent deleting files within a specific directory.
//        MultiThreadedTaskHandler taskHandler = new MultiThreadedTaskHandler("Vacuum clean", false);
//
//        for (final String cleanupDir : cleanupDirs)
//            taskHandler.Add(new Callable<TaskStatus>()
//            {
//                public TaskStatus call() throws Exception
//                {
//                try
//                {
//                    // Can't do this in a LockedCallable object because we are deleting directories/files that support it
//                    MiscUtilities.DeleteCoreDirectory(cleanupDir);
//                }
//                catch (Exception ex)
//                {
//                    Singletons.Log.Debug(ex);
//                }
//
//                return new TaskStatus(TaskStatusType.Completed);
//                }
//            });
//
//        taskHandler.ExecuteTasks();
    }
}

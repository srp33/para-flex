import java.util.ArrayList;

public class TaskStatus
{
    public TaskStatusType StatusType;
    public String MachineAddress;
    public String ProcessID;
//    public long Time;

    public TaskStatus(TaskStatusType statusType)
    {
        this(statusType, Singletons.MachineAddress, Singletons.ProcessID);
    }

    public TaskStatus(TaskStatusType statusType, String machineAddress, String processID)
    {
        StatusType = statusType;
        MachineAddress = machineAddress;
        ProcessID = processID;
//        Time = DateUtilities.GetCurrentDate().getTime();
    }

    public static boolean HasCompleted(ArrayList<TaskStatus> statuses)
    {
        for (TaskStatus status : statuses)
            if (status.StatusType.equals(TaskStatusType.Completed) || status.StatusType.equals(TaskStatusType.PreviouslyCompleted))
                return true;

        return false;
    }

    @Override
    public String toString()
    {
        return StatusType.name() + "\n" + MachineAddress + "\n" + ProcessID + "\n";
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
            return false;

        if (!(obj instanceof TaskStatus))
            return false;

        TaskStatus compareObj = (TaskStatus)obj;

        return compareObj.StatusType.equals(this.StatusType) && compareObj.MachineAddress.equals(this.MachineAddress) && compareObj.ProcessID.equals(this.ProcessID);
    }
}

import os, sys, glob

noFlexTimeFilePath = sys.argv[1]
singleFlexTimeFilePath = sys.argv[2]
multiFlexTimeFilePath = sys.argv[3]
singleFilePath = sys.argv[4]
multipleFilePattern = sys.argv[5]

def areAllNumeric(x):
    for y in x:
        try:
            z = float(y)
        except:
            return False

    return True

def addSystemElapsedTime(timeFilePath, theDict):
    systemTime1 = [line.rstrip().split("\t") for line in file(timeFilePath)][1][1]
    systemTime2 = [line.rstrip().split("\t") for line in file(timeFilePath)][2][1]
    systemTime3 = [line.rstrip().split("\t") for line in file(timeFilePath)][3][1]

    theDict[systemTimeKey1] = [parseSystemTimeSeconds(systemTime1)]
    theDict[systemTimeKey2] = [parseSystemTimeSeconds(systemTime2)]
    theDict[systemTimeKey3] = [parseSystemTimeSeconds(systemTime3)]

def parseSystemTimeSeconds(systemTime):
    minutes = float(systemTime.split("m")[0])
    seconds = float(systemTime.split("m")[1].replace("s", ""))

    return seconds + (minutes * 60.0)

def addValuesToDict(filePath, theDict):
    for line in file(filePath):
        lineItems = line.rstrip().split("\t")
        if len(lineItems) < 2:
            continue
        theDict[lineItems[0]] = theDict.setdefault(lineItems[0], []) + [lineItems[1]]
        uniqueKeys.add(lineItems[0])

systemTimeKey1 = "System real elapsed time (s)"
systemTimeKey2 = "System user elapsed time (s)"
systemTimeKey3 = "System sys elapsed time (s)"
uniqueKeys = set([systemTimeKey1, systemTimeKey2, systemTimeKey3])

dict1 = {}
dict2 = {}
dict3 = {}

addSystemElapsedTime(noFlexTimeFilePath, dict1)
addSystemElapsedTime(singleFlexTimeFilePath, dict2)
addSystemElapsedTime(multiFlexTimeFilePath, dict3)

addValuesToDict(singleFilePath, dict2)

for multipleFilePath in glob.glob(multipleFilePattern):
    addValuesToDict(multipleFilePath, dict3)

uniqueKeys = sorted(list(uniqueKeys))

print "Metric\tNoFlex\tSingleFlex\tMultiFlex"
for field in uniqueKeys:
    if " ID" in field:
        continue

    if not dict1.has_key(field):
        dict1[field] = ["NaN"]
    if not dict2.has_key(field):
        dict2[field] = ["0"]
    if not dict3.has_key(field):
        dict3[field] = ["0"]

    allValues = dict2[field] + dict3[field]

    if areAllNumeric(allValues):
        noflex = "%.3f" % float(dict1[field][0])
        single = "%.3f" % float(dict2[field][0])

        if "elapsed" in field or field == "Total commands":
            multi = "%.3f" % (max([float(x) for x in dict3[field]]))
        else:
            multi = "%.3f" % (sum([float(x) for x in dict3[field]]))
    else:
        noflex = str(dict1[field][0])
        single = str(dict2[field][0])
        multi = ",".join([str(x) for x in sorted(list(set(dict3[field])))])

    print "%s\t%s\t%s\t%s" % (field, noflex, single, multi)

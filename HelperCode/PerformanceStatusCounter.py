import os, sys, glob, posix, shutil, time
from datetime import datetime
from operator import itemgetter, attrgetter

for dirPath in glob.glob("Performance/*"):
    dirName = os.path.basename(dirPath)
    outFilePath = dirPath + "/Status.txt"

    statusRows = []
    for filePath in glob.glob(dirPath + "/Status*.txt"):
        for line in file(filePath):
            lineItems = line.rstrip().split("\t")
            statusRows.append(lineItems)

    outputRows = []
    for lineNumber in set([x[0] for x in statusRows]):
        lineNumberValues = [x for x in statusRows if x[0] == lineNumber]
        numCompleted = len([x for x in lineNumberValues if x[3] == "Completed" or x[3] == "UpdateCompletedStatusFailed" or x[3] == "DeleteLockFileFailed" or x[3] == "DeletePendingFileFailed"])
        if numCompleted > 1:
            lineNumberValues.sort(key=itemgetter(4))
            outputRows.extend(lineNumberValues)

    outFile = open(outFilePath, 'w')
    outFile.write("\n".join(["\t".join([str(x) for x in line]) for line in outputRows]))
    outFile.close()

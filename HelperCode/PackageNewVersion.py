import os, posix, shutil

versionFile = open("Para-Flex/Version.txt")
version = versionFile.readline().strip()
versionFile.close()

tarGzFilePath = "Para-Flex_%s.tar.gz" % version
posix.system("tar zcvf %s Para-Flex" % tarGzFilePath)
shutil.copy(tarGzFilePath, "Para-Flex/" + tarGzFilePath)

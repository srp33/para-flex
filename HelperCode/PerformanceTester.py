import os, sys, glob, posix, shutil, time
from datetime import datetime

taskSeriesFilePathMain = sys.argv[1]
numProcessesOptions = [int(x) for x in sys.argv[2].split(",")]
numThreadsOptions = [int(x) for x in sys.argv[3].split(",")]

commandTemplate = "java -Xmx3g -jar paraflex.jar ACTION=%s TASK_SERIES_FILE=%s DEBUG=true MONITOR=true NUM_THREADS=%i PAUSE_SECONDS=0 THREAD_TIMEOUT_MINUTES=1"

#print "Executing " + taskSeriesFilePathMain + " in serial"
#start = datetime.now()
#posix.system(taskSeriesFilePathMain)
#diff = datetime.now() - start
###writeOutput(["Serial", 1, diff.total_seconds(), diff.total_seconds(), "0.0"])

for numProcesses in numProcessesOptions:
    for numThreads in numThreadsOptions:
        taskSeriesFilePath = taskSeriesFilePathMain + "_%iProcesses_%iThreads" % (numProcesses, numThreads)
        shutil.copy(taskSeriesFilePathMain, taskSeriesFilePath)

        commandText = "#!/bin/bash\n"
        for i in range(numProcesses):
            commandText += (commandTemplate + " &\n") % ("Process", taskSeriesFilePath, numThreads)
        commandText += "wait"

        posix.system(commandText)

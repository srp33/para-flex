import os, sys, glob, posix, shutil, time
from datetime import datetime

outFilePath = sys.argv[1]

outputLines = []
outputKeys = ["Number processes", "Number threads"]

for dirPath in glob.glob("Performance/*"):
    description = os.path.basename(dirPath)
    numProcesses = description.split("_")[1].replace("Processes", "")
    numThreads = description.split("_")[2].replace("Threads", "")

    for filePath in glob.glob(dirPath + "/Report*.txt"):
        outputValues = []

        for line in file(filePath):
            lineItems = line.rstrip().split("\t")
            key = lineItems[0]
            value = lineItems[1]

            if key not in outputKeys:
                outputKeys.append(key)

            outputValues.append(value)

        outputLines.append([numProcesses, numThreads] + outputValues)

outputLines.insert(0, outputKeys)

outFile = open(outFilePath, 'w')
outFile.write("\n".join(["\t".join([str(x) for x in line]) for line in outputLines]))
outFile.close()
